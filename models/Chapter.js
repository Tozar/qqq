const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChapterSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    articlesInStore: [{type: Schema.Types.ObjectId, ref: 'Article'}],
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Chapter = mongoose.model('Chapter', ChapterSchema);