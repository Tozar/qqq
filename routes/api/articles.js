const express = require ('express');
const router = express.Router();

const Article = require ('../../models/Article');
const Chapter = require ('../../models/Chapter');



// POST
router.post('/:chapterId', auth, (req, res) => {
    const chapterID = req.params.chapterId;
    Chapter.findOne({_id: chapterID})
        .then(chapter=> {
            var article = new Article({text: req.body.text});
            article.save().then(() => {
                chapter.articlesInStore.push(article);
                chapter.save().then(newChapter=>res.json(newChapter));
            })
        })   
});

router.delete('/:chapterID/:id', auth, (req, res) => {
    const id = req.params.id;
    const chapterID = req.params.chapterID;
    // Article.findByIdAndRemove(id)
    //     .then(article => res.json(article));
    Article.findById(id)
        .then(()=> {
            Chapter.findOneAndUpdate(
                {_id: chapterID},
                {$pull: { articlesInStore: id  } })
                .then(chapter => res.json(chapter));
        })
        
});




module.exports = router;