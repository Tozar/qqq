const express = require ('express');
const router = express.Router();
const auth = require ('../../middleware/auth');

const Chapter = require ('../../models/Chapter');

// GET
router.get('/', (req, res) => {
    Chapter.find()
        .sort ({date: -1})
        .then(chapters => res.json(chapters));
});

// POST
router.post('/', auth, (req, res) => {

    const newChapter = new Item ({
        name: req.body.name
    });

    newChapter.save().then(chapter => res.json(chapter));

});

//DELETE
router.delete('/:id', auth, (req, res) => {
    const id = req.params.id;
    Chapter.findById(id)
        .then(chapter=> chapter.remove().then(()=> res.json({success: true})))
});



module.exports = router;