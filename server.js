const express = require ('express');
const mongoose = require ('mongoose');

const app = express();

app.use(express.json());

// Routes to the back-end
app.use('/api/chapters', require ('./routes/api/chapters'));
app.use('/api/users', require ('./routes/api/users'));
app.use('/api/articles', require ('./routes/api/articles'));


// Creating server on port 5000
mongoose
    .connect("mongodb://localhost:27017/items", {useNewUrlParser: true}, function(err){
    if (err)
        return console.log(err);

    app.listen(5000, function(){
        console.log("The server started to operate");
    });
});
